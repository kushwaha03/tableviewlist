//
//  expandViewController.swift
//  forAll
//
//  Created by karmaa lab on 09/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit

class expandViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var dateCellExpanded: Bool = false

    @IBOutlet weak var expandtable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! labelTableViewCell
//        cell.textLabel?.text = "abc"
        cell.moreLessbtn.tag = indexPath.row
        cell.moreLessbtn.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
        if (indexPath.row%2 == 0) {
            cell.activestatus.backgroundColor = .red
            
        }
//        cell.activestatus.backgroundColor = .red
        return cell
    }
    @objc func pressButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
//        if dateCellExpanded {
//            dateCellExpanded = false
//             print("Button  clicked! for unexapand")
//        } else {
//            dateCellExpanded = true
//            print("Button  clicked! for exapand")
//        }
//      self.expandtable.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! labelTableViewCell
//        print(cell.moreLessbtn.tag)
//        return 200.0
        if indexPath.row == 0 {
            if dateCellExpanded {
                return 350.0
            } else {
                return 200.0
            }
        }
        return 200.0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! labelTableViewCell
//
        if indexPath.row == 0 {
            if dateCellExpanded {
                dateCellExpanded = false
            } else {
                dateCellExpanded = true
            }
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
