//
//  TableViewController.swift
//  forAll
//
//  Created by karmaa lab on 09/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var showlabel: UILabel!
    var array_Rnew = [[String : Any]]()
    var array_R: [String] = []
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parseData()
        // Do any additional setup after loading the view.
    }
    
    func parseData()
    {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/users") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
//                print(jsonResponse) //Response result
                guard let jsonArray = jsonResponse as? [[String: Any]] else {
                    return
                }
                print("jsonArray", jsonArray)
                self.array_Rnew = jsonArray
                //Now get title value
//                guard let title = jsonArray[0]["title"] as? String else { return }
               print(self.array_Rnew) // delectus aut autem
//                for title in jsonArray {
////                    print(title["title"] as Any)
//                    if let tit = title["title"] as? String {
////                        print(tit)
//                        self.array_R.append(tit)
//                    }
//
//                }
//                print(self.array_R.count)
//                self.count = self.array_R.count
//                 print(self.count)
                self.tableview.reloadData()
                
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.array_Rnew.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//                let cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! labelTableViewCell
//        print(self.array_R.count)
//        cell.textLabel?.text = " Name: " +  (self.array_Rnew[indexPath.row]["name"] as? String)! + " " + " UserName: " + (self.array_Rnew[indexPath.row]["username"] as? String)!
//        cell.labelview.backgroundColor = .red
        cell.labelview.text = "  Name: " +  (self.array_Rnew[indexPath.row]["name"] as? String)! + "\n " + " UserName: " + (self.array_Rnew[indexPath.row]["username"] as? String)! + "\n " + " Phone: " +  (self.array_Rnew[indexPath.row]["phone"] as? String)! + " " + "\n " +  " Email: " + (self.array_Rnew[indexPath.row]["email"] as? String)!
//        cell.textLabel?.text = self.array_Rnew[indexPath.row]["title"] as? String
//            cell.labelview.textAlignment = .center
//        cell.textLabel?.text = self.array_Rnew[indexPath.row]["name"] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 150.0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
